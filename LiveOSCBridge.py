import Live
from _Framework.ControlSurface import ControlSurface
from .constants import *
from .OSCHelper import OSCHelper
from .ActionHandler import ActionHandler
import threading

class LiveOSCBridge(ControlSurface):
    
    def __init__(self, c_instance):
        self.queue = []
        super(LiveOSCBridge, self).__init__(c_instance)
        self.__c_instance = c_instance
        with self.component_guard():
            self.OSCHelper = OSCHelper('127.0.0.1', 9001, 0, self.log_message)
            self.OSCHelper.setOSCHandler('default', self.queue_osc_message)
            self.OSCHelper.initOSCClient('127.0.0.1', 54341)
        self.__action_handler = ActionHandler(self, self.song())
        self.message_timer = Live.Base.Timer(callback=self.check_queue, interval=20, repeat=True)
        self.message_timer.start()

    def connect(self):
        self.log_message("LiveOSCBridge Connected")
        self.initialize()

    def refresh_state(self):
        self.initialize()

    def disconnect(self):
        self.OSCHelper.disconnect()
        self.message_timer.stop()

    def initialize(self):
        self.log_message("in initialization, sending scenes")
        self.OSCHelper.sendOSCMsg('/scene_begin')
        for n, scene in enumerate(self.song().scenes):
            if scene.name:
                self.OSCHelper.sendOSCMsg('/scene', [n, scene.name])
        self.OSCHelper.sendOSCMsg('/scene_end')

    def queue_osc_message(self, addr, tags, data,source):
    # Handle all OSC messages in default handler
        self.queue.append(ParsedOSCMessage(addr, tags, data, source))
          
    def check_queue(self):
        for n, message in enumerate(self.queue):
            self.log_message("Message Received. Data: " + str(message.data) + " Addr:" + str(message.addr) + " Tags:" + str(message.tags) + " Source:" + str(message.source))
            getattr(self.__action_handler, self.get_method(message.addr))(message.tags, message.data, message.source)
            del self.queue[n]
    @staticmethod
    def get_method(argument):
        action_map = {
            '/live/metronome' : 'metronome',
            '/live/tempo' : 'change_tempo',
            '/live/launch_deck_a' : 'launch_deck_a'
        }
        return action_map.get(argument)
class ParsedOSCMessage:
    def __init__(self, addr, tags, data, source):
        self.addr = addr
        self.tags = tags
        self.data = data
        self.source = source


class ActionHandler:
    def __init__(self, parent, song):
        self.parent = parent
        self.song = song

    def log_message(self, message):
        self.parent.log_message(message)

    def metronome(self, tags, data, source):
        self.log_message("metronome")
        pass
    
    def change_tempo(self, tags, data, source):
        self.log_message("tempo")
        pass

    def launch_deck_a(self, tags, data, source):
        self.log_message("launching scene" + str(data[0]))
        self.song.scenes[int(data[0])].fire()
        pass